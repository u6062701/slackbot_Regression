import os 
import json
def readTestharness(keyword):
	if keyword.lower() in "test1":
		listfail = []
		tsi6fail = 0
		tsi6pass = 0
		tsi7fail = 0
		tsi7pass = 0
		logpath = '..\TSI6_TestHarnessLog_TestMachine1.dat'
		logTSI6 = '..\TSI6_TestMachine1.dat'
		logTSI7 = '..\TSI7_TestMachine1.dat'
		temp = []
		
		with open(logpath) as f:
			for readline in f :
				listfail.append(readline)	
		with open(logTSI6) as f:
			for readline in f :
				if "Fail" in readline:
					tsi6fail += 1 
				else :
					tsi6pass += 1
		with open(logTSI7) as f :
			for readline in f:
				if "Fail" in readline:
					tsi7fail += 1 
				else:
					tsi7pass += 1
 		return listfail,tsi6fail,tsi6pass,tsi7fail,tsi7pass
	elif keyword.lower() in "test2":
		listfail = []
		logpath = '..\TSI6_TestHarnessLog_TestMachine2.dat'
		logTSI6 = '..\TSI6_TestMachine2.dat'
		logTSI7 = '..\TSI7_TestMachine2.dat'
		tsi6fail = 0
		tsi6pass = 0
		tsi7fail = 0
		tsi7pass = 0
		testsuite = []
		testcase = []
		with open(logpath) as f:
			for readline in f:
			#print readline
				listfail.append(readline)
			
		with open(logTSI6) as f:
			realtemp = []
			realtemp1 =[]
			realtemp2 = []
			testsuite = []
			temp = ''
			testsuite_count =0
			data = {}
			data['testcase'] =[]
			data['testsuite'] = []
			data['count'] = []
			string_json = 
			for readline in f:
				if "Fail" in readline:
					tsi6fail += 1 
					realtemp = readline.split("','")
					realtemp1 = realtemp[0].split("('")
					realtemp2 = realtemp[1]
					testcase.append(realtemp2)
					testsuite.append(realtemp1[1])
					#print 'realtemp1: '+str(realtemp1[1])
					#if realtemp == realtemp[len(realtemp)-1]:	
					#temp.append(realtemp1[1])			
					if realtemp1[1] == temp:
						testsuite_count += 1 
					else :
						string_json += 
						testsuite_count = 1
						temp = realtemp1[1]
					#print realtemp1[1]+" count : "+str(testsuite_count)
					#json_data = json.dumps(data)
					print json_data
					f=open('test_json.json','w')
					f.write(json_data)
					f.close()
					
					del realtemp1[:]
				else :
					tsi6pass += 1
					
		with open(logTSI7) as f :
			for readline in f:
				if "Fail" in readline:
					tsi7fail += 1 
				else:
					tsi7pass += 1
 		return listfail,tsi6fail,tsi6pass,tsi7fail,tsi7pass,testsuite,testcase